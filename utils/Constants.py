from enum import Enum

class ConnectionType(Enum):
    SSH = 1
    TELNET = 2